const path = require('path');

module.exports = {
	devtool: 'source-map',
	entry: path.join(__dirname, 'src', 'index'),
	output: {
		path: path.join(__dirname, 'dist'),
		filename: 'bundle.js',
		// library: ['Plugsite'],
		libraryTarget: 'umd',
		publicPath: '/dist/'
	},
	module: {
		rules: [{
			test: /\.(js|jsx)$/,
			exclude: /node_modules/,
			include: /src/,
			loader: 'babel-loader',
			options: {
				plugins: [
					['@babel/transform-react-jsx', { pragma: 'h' }],
					['@babel/plugin-proposal-class-properties', { loose: true }]
				]
			}
		}]
	},
	resolve: {
		alias: {
		  react: 'preact/compat',
		  'react-dom': 'preact/compat'
		}
	}
};