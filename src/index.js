import { h, render } from 'preact';
import HelloWorld from './components/hello-world';

render(<HelloWorld />, document.getElementById('app'));